import React from 'react';
import { View, StyleSheet, Animated} from 'react-native';
import MapView from 'react-native-maps';
import GeoCoder from 'react-native-geocoder';
import LocationPin from './component/LocationPin';
import LocationSearch from './component/LocationSearch';
import ClassSelection from './component/ClassSelection';
import ConfirmationModal from './component/ConfirmationModal';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            position: null,
            carLocation: [
                {
                    rotation: 78,
                    latitude: 37.78725,
                    longitude: -122.4318
                },
                {
                    rotation: -10,
                    latitude: 37.79015,
                    longitude: -122.4318,
                },
                {
                    rotation: 262,
                    latitude: 37.78525,
                    longitude: -122.4348,
                },
            ],
        };
        this.initialRegion = {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421,
        };
    }

    _onRegionChange(region) {
        this.setState({ position: null });
        const self = this;
        if (this.timeoutID) clearTimeout(this.timeoutID);
        this.timeoutID = setTimeout(async () => {
            try {
                const res = await GeoCoder.geocodePosition({
                    lat: region.latitude,
                    lng: region.longitude,
                });
                self.setState({ position: res[0] });
            } catch (err) {
                console.log(err);
            }
        }, 2000);
    }

    componentDidMount() {
        this._onRegionChange.call(this, this.initialRegion);
    }

    _onBookingRequset() {
        this.setState({
            confirmationModalVisible: true,
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <MapView
                    style={styles.fullScreenMap}
                    initialRegion={this.initialRegion}
                    onRegionChange={this._onRegionChange.bind(this)}
                />
                {this.state.carLocation.map((carLocation, i) => (
                    <MapView.Marker key={i} coordinate={carLocation}>
                        <Animated.image
                            style={{
                                transform: [{ rotate: `${carLocation.rotation}deg` }],
                            }}
                            source={require('../img/car.png')}
                        />
                    </MapView.Marker>
                ))}
                <LocationSearch
                    value={
                        this.state.position && (this.state.position.feature || this.state.position.formattedAddress)
                    }
                />
                <LocationPin onPress={this._onBookingRequset.bind(this)} />
                <ClassSelection />
                <ConfirmationModal
                    visible={this.state.confirmationModalVisible}
                    onClose={() => {
                        this.state({ confirmationModalVisible: false });
                    }}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    fullScreenMap: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
});

export default Main;